(() => {
	'use strict';

	angular.module('es', [
		'angular-loading-bar',
		'ngSanitize',
		'restangular',
		'ui.bootstrap',
		'ui.router'
	]);
})();
